package org.example.factory;

import org.junit.Assert;
import org.junit.Test;

public class ShapeFactoryTest {
    @Test
    public void should_instantiate_a_circle_when_number_of_sides_is_1() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(1, 1 * Math.PI * 2);
            Assert.assertTrue("Should be an instance of a Circle", shape instanceof Circle);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_area_for_a_circle() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(1, 3 * Math.PI * 2);
            Assert.assertEquals("Should give the correct area for a circle", shape.getArea(), Math.PI * Math.pow(3, 2), 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_perimeter_for_a_circle() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(1, 5 * Math.PI * 2);
            Assert.assertEquals("Should give the correct perimeter for a circle", shape.getPerimeter(), 2 * Math.PI * 5, 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_instantiate_an_equilateral_triangle_when_number_of_sides_is_3() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(3, 5);
            Assert.assertTrue("Should be an instance of an EquilateralTriangle", shape instanceof EquilateralTriangle);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_area_for_an_equilateral_triangle() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(3, 4);
            Assert.assertEquals("Should give the correct area for an equilateral triangle", shape.getArea(), Math.sqrt(3) / 4 * Math.pow(4, 2), 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_perimeter_for_an_equilateral_triangle() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape circle = shapeFactory.getShape(3, 5);
            Assert.assertEquals("Should give the correct perimeter for an equilateral triangle", circle.getPerimeter(), 15, 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_instantiate_a_square_when_number_of_sides_is_4() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(4, 1);
            Assert.assertTrue("Should be an instance of a Square", shape instanceof Square);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_area_for_a_square() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(4, 3);
            Assert.assertEquals("Should give the correct area for a square", shape.getArea(), 9, 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test
    public void should_give_the_correct_perimeter_for_a_square() {
        ShapeFactory shapeFactory = new ShapeFactory();
        try {
            Shape shape = shapeFactory.getShape(4, 5);
            Assert.assertEquals("Should give the correct perimeter for a square", shape.getPerimeter(), 20, 0.00001);
        } catch (Exception e) {
            Assert.fail("Should instantiate a valid Shape");
        }
    }

    @Test(expected = Exception.class)
    public void should_throw_an_exception_when_instantiating_a_shape_with_2_sides() throws Exception {
        ShapeFactory shapeFactory = new ShapeFactory();
        shapeFactory.getShape(2, 5);
    }

    @Test(expected = Exception.class)
    public void should_throw_an_exception_when_instantiating_a_shape_with_more_than_4_sides() throws Exception {
        ShapeFactory shapeFactory = new ShapeFactory();
        shapeFactory.getShape(5, 1);
    }
}
