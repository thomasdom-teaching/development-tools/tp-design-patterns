package org.example.strategy;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

@SuppressWarnings("rawtypes")
public class PaymentStrategyTest {
    @Test
    public void should_have_a_PaymentStrategy_interface() {
        try {
            Class c = Class.forName("org.example.strategy.PaymentStrategy");
            Assert.assertEquals("Should be named PaymentStrategy", "PaymentStrategy", c.getSimpleName());
            Assert.assertTrue("Should be an interface", c.isInterface());
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create an interface called 'PaymentStrategy'");
        }
    }

    @Test
    public void should_have_a_PaymentStrategy_interface_implementing_an_abstract_method_pay() {
        try {
            Class c = Class.forName("org.example.strategy.PaymentStrategy");
            Optional<Method> optionalPayMethod = Arrays.stream(c.getMethods()).filter(m -> m.getName().equals("pay")).findFirst();
            Assert.assertTrue("Should declare a pay method", optionalPayMethod.isPresent());
            Method payMethod = optionalPayMethod.get();
            Assert.assertEquals("Should have only one parameter", payMethod.getParameterCount(), 1);
            Assert.assertEquals("Should be of type 'double' for parameter", payMethod.getParameterTypes()[0], Double.TYPE);
            Assert.assertEquals("Should return a double", payMethod.getReturnType(), Double.TYPE);
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create an interface called 'PaymentStrategy'");
        }
    }

    @Test
    public void should_have_a_CreditCardPaymentStrategy_class() {
        try {
            Class c = Class.forName("org.example.strategy.CreditCardPaymentStrategy");
            Assert.assertEquals("Should be named CreditCardPaymentStrategy", "CreditCardPaymentStrategy", c.getSimpleName());
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create a class called 'CreditCardPaymentStrategy'");
        }
    }

    @Test
    public void should_implement_the_PaymentStrategy_interface_for_CreditCardPaymentStrategy_class() {
        try {
            Class c = Class.forName("org.example.strategy.CreditCardPaymentStrategy");
            Assert.assertTrue(
                    "Should implement the interface PaymentStrategy",
                    Arrays.stream(c.getInterfaces())
                            .map(Class::getSimpleName)
                            .anyMatch(n -> n.equals("PaymentStrategy"))
            );
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create a class called 'CreditCardPaymentStrategy'");
        }
    }

    @Test
    public void should_return_the_correct_amount_for_method_pay_of_CreditCardPaymentStrategy_class() {
        PaymentStrategy creditCardPaymentStrategy = new CreditCardPaymentStrategy();
        double payment = creditCardPaymentStrategy.pay(150);
        Assert.assertEquals("Should return the amount + 15", payment, 165, 0.00001);
    }

    @Test
    public void should_have_a_CashPaymentStrategy_class() {
        try {
            Class c = Class.forName("org.example.strategy.CashPaymentStrategy");
            Assert.assertEquals("Should be named CashPaymentStrategy", "CashPaymentStrategy", c.getSimpleName());
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create a class called 'CashPaymentStrategy'");
        }
    }

    @Test
    public void should_implement_the_PaymentStrategy_interface_for_CashPaymentStrategy_class() {
        try {
            Class c = Class.forName("org.example.strategy.CashPaymentStrategy");
            Assert.assertTrue(
                    "Should implement the interface PaymentStrategy",
                    Arrays.stream(c.getInterfaces())
                            .map(Class::getSimpleName)
                            .anyMatch(n -> n.equals("PaymentStrategy"))
            );
        } catch (ClassNotFoundException e) {
            Assert.fail("Should create a class called 'CashPaymentStrategy'");
        }
    }

    @Test
    public void should_return_the_correct_amount_for_method_pay_of_CashPaymentStrategy_class() {
        PaymentStrategy creditCardPaymentStrategy = new CashPaymentStrategy();
        double payment = creditCardPaymentStrategy.pay(150);
        Assert.assertEquals("Should return the amount + 5", payment, 155, 0.00001);
    }
}
